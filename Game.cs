﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace Asteroids
{
    static class Game
    {
        static BufferedGraphicsContext ctx;
        static public BufferedGraphics buffer;
        static public int Width { get; set; }
        static public int Height { get; set; }

        static Ship ship;
        static List<Bullet> bullets = new List<Bullet>();
        static List<Asteroid> asteroids = new List<Asteroid>();
        static Aid aid;

        private static int score = 0;
        private static int asteroidsCount = 1;
        private static int shipX = 10;
        private static int aidY;

        static Timer timer = new Timer();
        public static Random rnd = new Random();


        static Game()
        {
        }
    static public void Init(Form form)
        {
            Graphics g;
            ctx = BufferedGraphicsManager.Current;
            Load();
            g = form.CreateGraphics();
            Width = form.Width;
            Height = form.Height;
            buffer = ctx.Allocate(g, new Rectangle(0, 0, Width, Height));
            timer.Interval = 100;
            timer.Start();
            timer.Tick += Timer_Tick;
            form.KeyDown += Form_KeyDown;
            Ship.MessageDie += Finish;
            Ship.MessageEnergyChanging += Log;
            Aid.AidSpawn += Log;
        }
        static public void Draw()
        {
            buffer.Graphics.Clear(Color.Black);
            foreach (Asteroid a in asteroids) a?.Draw();
            foreach (Bullet b in bullets) b.Draw();
            ship.Draw();
            aid?.Draw();
            buffer.Graphics.DrawString("Energy:" + ship.Energy + ", score: " + score, SystemFonts.DefaultFont, Brushes.White, 0, 0);
            buffer.Render();
        }
        static public void Update()
        {
            foreach (Bullet b in bullets) b.Update();
            aid?.Update();
            if (asteroids.Count == 0)
            {
                asteroidsCount++;
                for (int i = 0; i < asteroidsCount; i++)
                {
                    asteroids.Add(new Asteroid(new Point(600, i * 20), new Point(-i, 0), new Size(20, 20)));
                }
            }
            for (int i = 0; i < asteroids.Count; i++)
            {
                asteroids[i].Update();
                if (ship.Collision(asteroids[i]))
                {
                    ship.EnergyLow(rnd.Next(1, 10));
                    System.Media.SystemSounds.Asterisk.Play();
                    if (ship.Energy <= 0) ship.Die();
                }
                for (int j = 0; j < bullets.Count; j++)
                {
                    if (bullets[j].Position.X >= Game.Width)
                    {
                        bullets.RemoveAt(j);
                        continue;
                    }
                    if (asteroids.Count != 0 && bullets[j].Collision(asteroids[i]))
                    {
                        System.Media.SystemSounds.Hand.Play();
                        asteroids.RemoveAt(i);
                        bullets.RemoveAt(j);
                        score++;
                        i = i - 1 < 0 ? 0 : i - 1;
                        j--;
                        continue;
                    }
                }
            }
            if (aid != null)
            {
                if(aid.ttl < 0)
                {
                    aid = null;
                    return;
                }
                if(ship.Collision(aid))
                {
                    ship.EnergyUp(10);
                    aid = null;
                }
            }
            if (Game.rnd.Next(0, 100) > 85 && aid == null)
            {
                aidY = Game.rnd.Next(0, Game.Height);
                aid = new Aid(new Point(shipX, aidY), new Point(5, 5), new Size(10, 10));
            }
        }
        static public void Load()
        {
            ship = new Ship(new Point(shipX, 400), new Point(5, 5), new Size(10, 10));
            for(int i = 0; i < asteroidsCount; i++)
            {
                asteroids.Add(new Asteroid(new Point(600, i * 20), new Point(-i, 0), new Size(20, 20)));
            }
        }
        private static void Timer_Tick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }
        static private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                bullets.Add(new Bullet(new Point((int)ship.Rect.X + 10, (int)ship.Rect.Y + 4), new Point(4, 0), new Size(4, 1)));
            }
            if (e.KeyCode == Keys.Up) ship.Up();
            if (e.KeyCode == Keys.Down) ship.Down();
        }
        static public void Finish()
        {
            timer.Stop();
            buffer.Graphics.DrawString("The End", new Font(FontFamily.GenericSansSerif, 60, FontStyle.Underline),
            Brushes.White, 200, 100);
            buffer.Render();
        }        static private void Log(string Name, string Message)
        {
            Console.WriteLine($"Name: {Name};Message: {Message}");
        }
    }
}
