﻿using System;
using System.Drawing;

namespace Asteroids
{
    class Aid : BaseObject
    {
        public int ttl = 30;
        public Aid(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }
        public override void Draw()
        {
            Image boom = Image.FromFile("red_cross.jpg");
            Game.buffer.Graphics.DrawImage(boom, pos.X, pos.Y, size.Width, size.Height);
            AidSpawn?.Invoke("Aid", $"Spawned at {pos.X},{pos.Y}");
        }
        public override void Update()
        {
            ttl--;
        }
        public static event Logger AidSpawn;
    }
}
