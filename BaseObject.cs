﻿using System;
using System.Drawing;

namespace Asteroids
{
    interface ICollision
    {
        bool Collision(ICollision obj);
        RectangleF CollisionRect { get; }
    }

    abstract class BaseObject : ICollision
    {
        public RectangleF Rect => GetRect();
        public RectangleF CollisionRect => GetCollisionRect();

        protected Point pos;
        protected Point dir;
        protected Size size;
        public Point Position
        {
            get
            {
                return this.pos;
            }
        }

        public Size Size
        {
            get
            {
                return this.size;
            }
        }
        public BaseObject(Point pos, Point dir, Size size)
        {
            this.pos = pos;
            this.dir = dir;
            this.size = size;
        }
        abstract public void Draw();
        abstract public void Update();

        protected virtual RectangleF GetRect()
        {
            return new RectangleF(pos, size);
        }
        protected virtual RectangleF GetCollisionRect()
        {
            return new RectangleF(pos, size);
        }
        public bool Collision(ICollision obj)
        {
            bool res = obj.CollisionRect.IntersectsWith(CollisionRect);
            return res;
        }
        protected virtual void Log()
        {

        }
        public delegate void Message();
        public delegate void Logger(string Name, string Message);

    }
}
