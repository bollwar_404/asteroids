﻿using System;
using System.Drawing;

namespace Asteroids
{
    class Ship : BaseObject
    {
        private int energy = 100;
        public int Energy
        {
            get { return energy; }
        }
        public void EnergyLow(int n)
        {
            energy -= n;
            MessageEnergyChanging?.Invoke("ship", $"Energy down for {n}");
        }
        public void EnergyUp(int n)
        {
            energy += n;
            MessageEnergyChanging?.Invoke("ship", $"Energy up for {n}");
        }
        public Ship(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }
        public override void Draw()
        {
            Game.buffer.Graphics.FillEllipse(Brushes.Wheat, pos.X, pos.Y, size.Width, size.Height);
        }
        public override void Update()
        {
        }
        public void Up()
        {
            if (pos.Y > 0) pos.Y = pos.Y - dir.Y;
        }
        public void Down()
        {
            if (pos.Y < Game.Height) pos.Y = pos.Y + dir.Y;
        }
        public void Die()
        {
            MessageDie?.Invoke();
        }
        public static event Message MessageDie;
        public static event Logger MessageEnergyChanging;
    }
}
