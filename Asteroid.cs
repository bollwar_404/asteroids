﻿using System;
using System.Drawing;

namespace Asteroids
{
    class Asteroid : BaseObject
    {
        public Asteroid(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
        }
        public override void Draw()
        {
            Image boom = Image.FromFile("boom.jpg");
            Game.buffer.Graphics.DrawImage(boom, pos.X, pos.Y, size.Width, size.Height);
            Game.buffer.Graphics.DrawLine(Pens.White, pos.X + (size.Width / 2), pos.Y, pos.X + size.Width, pos.Y - size.Height);
            Game.buffer.Graphics.DrawLine(Pens.White, pos.X + size.Width / 4 * 3, pos.Y + size.Height / 4, pos.X + size.Width + (size.Width / 3 * 2), pos.Y - size.Height);
            Game.buffer.Graphics.DrawLine(Pens.White, pos.X + size.Width, pos.Y + size.Height / 2, pos.X + size.Width / 4 * 9, pos.Y - size.Height);
        }
        public override void Update()
        {
            pos.X = pos.X + dir.X / 2;
            pos.Y = pos.Y + 10;
            if (pos.X < 0) Respawn();
            if (pos.Y > Game.Height - 10) Respawn();
        }
       public void Respawn()
        {
            pos.X = Game.rnd.Next(0, Game.Width);
            pos.Y = 0;
        }
    }
}
